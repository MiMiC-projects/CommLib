#  Setup functions
function(setup_c_test target_name target_file)
  add_executable(${target_name} ${target_file})
  target_link_libraries(${target_name}
    mimiccomm
    stdc++
    ${CMAKE_THREAD_LIBS_INIT}
  )
  set_target_properties(${target_name}
    PROPERTIES
    LANGUAGE C
    LINKER_LANGUAGE C
  )
endfunction()

function(setup_gtest_test target_name target_file)
  add_executable(${target_name} ${target_file})
  target_include_directories(${target_name}
    PUBLIC
    ${GTEST_INCLUDE_DIRS}
    ${GMOCK_INCLUDE_DIRS}
  )
  target_link_libraries(${target_name}
    mimiccomm
    ${GTEST_BOTH_LIBRARIES}
    ${GMOCK_BOTH_LIBRARIES}
    ${CMAKE_THREAD_LIBS_INIT}
  )
  set_target_properties(${target_name}
    PROPERTIES
    LANGUAGE CXX
    LINKER_LANGUAGE CXX
  )
endfunction()

# run functions
function(run_single target_name)
  add_test(
    NAME ${target_name}
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMAND ${target_name}
  )
  add_dependencies(tests ${target_name})
endfunction()

function(run_mpi target_name np)
  add_test(
    NAME ${target_name}
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMAND ${MPIEXEC_EXECUTABLE} ${MPIEXEC_NUMPROC_FLAG} ${np} ${MPIEXEC_PREFLAGS}
            ./${target_name} ${MPIEXEC_POSTFLAGS}
  )
  set_tests_properties(${target_name}
    PROPERTIES ENVIRONMENT "MCL_PORT_DIR=${CMAKE_CURRENT_BINARY_DIR}")
endfunction()

function(run_mpmd target_name np1 np2 np3)
  add_test(
    NAME ${target_name}
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMAND ${MPIEXEC_EXECUTABLE}
      ${MPIEXEC_NUMPROC_FLAG} ${np1} ${MPIEXEC_PREFLAGS} ./${target_name} ${MPIEXEC_POSTFLAGS} :
      ${MPIEXEC_NUMPROC_FLAG} ${np2} ${MPIEXEC_PREFLAGS} ./${target_name} ${MPIEXEC_POSTFLAGS} :
      ${MPIEXEC_NUMPROC_FLAG} ${np3} ${MPIEXEC_PREFLAGS} ./${target_name} ${MPIEXEC_POSTFLAGS}
  )
endfunction()
