# definitions for data types
add_compile_definitions(
  PP_TYPE_CHAR_VALUE=1
  PP_TYPE_INT_VALUE=2
  PP_TYPE_LONG_INT_VALUE=3
  PP_TYPE_FLOAT_VALUE=4
  PP_TYPE_DOUBLE_VALUE=5
)
