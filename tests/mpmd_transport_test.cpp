//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mpmd_transport.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "mpi.h"

#include "data_types.h"

using namespace mcl;
using ::testing::TestEventListeners;
using ::testing::UnitTest;
using ::testing::ElementsAreArray;
using ::testing::Not;
using ::testing::ExitedWithCode;

class MPMDTransportTest : public ::testing::Test {
protected:

    static MPI_Comm       world_comm; // MPI_COMM_WORLD
    static MPMDTransport  transport;

    MPI_Comm  local_comm; //
    int       rank;       // rank in local_comm
    int       program_id; // 

    static void SetUpTestSuite() { world_comm = MPI_COMM_WORLD; }

    void SetUp() override {
        MPI_Barrier(world_comm);
        local_comm = world_comm;
        transport = MPMDTransport(world_comm);
        int result = transport.initialize(&local_comm);
        ASSERT_EQ(result, 0);
        MPI_Comm_rank(local_comm, &rank);

    //    cAA use this once we implement ... in CMake
    //    program_id = get_env_var_int("MCL_PROG_NO");
    //    ASSERT_EQ(program_id, transport.getProgramId());
        program_id = transport.getProgramId();
    }
};

MPI_Comm      MPMDTransportTest::world_comm;
MPMDTransport MPMDTransportTest::transport(world_comm);
 

TEST_F(MPMDTransportTest, Initialize) {
    int num_programs = transport.getNPrograms();
    ASSERT_EQ(num_programs, 3);

    // server
    if (program_id == 0) {
        ASSERT_FALSE(transport.isClient());
        ASSERT_TRUE((transport.isServer()) == (rank == 0));
    } 
    // clients
    if (program_id == 1 || program_id == 2) {
        ASSERT_FALSE(transport.isServer());
        ASSERT_TRUE((transport.isClient()) == (rank == 0));
    }
}

TEST_F(MPMDTransportTest, PureVirtual) {
    ASSERT_EQ(transport.connect(std::rand()), 0);
    ASSERT_EQ(transport.acceptConnection(std::rand()), 0);
    transport.closeConnection(std::rand());
    transport.destroy(std::rand());
}

TEST_F(MPMDTransportTest, Probe) {
    constexpr int send_size[2] = {5, 3};

    if (rank == 0) {
        // server
        if (program_id == 0) {
            for (size_t i = 1; i <= 2; i++) {
                int size = transport.probe(i, TYPE_INT);
                ASSERT_EQ(size, send_size[i - 1]);
                int* recv_buffer = new int[size];
                transport.receive(recv_buffer, size, TYPE_INT, 0, i);
                delete[] recv_buffer;
            }
        }
        // clients
        if (program_id == 1 || program_id == 2) {
            std::vector<int> send_data(send_size[program_id - 1]);
            transport.send(send_data.data(), send_data.size(), TYPE_INT, 0, 0);
        }
    }
}

TEST_F(MPMDTransportTest, PingPong) {
    int     ref_data_int[5] = {1, 2, 3, 4, 5};
    float   ref_data_flt[5] = {1.1, 2.2, 3.3, 4.4, 5.5};
    double  ref_data_dbl[5] = {1.1, 2.2, 3.3, 4.4, 5.5};

    // server
    if (program_id == 0) {
        for (size_t i = 1; i <= 2; i++) {
            int     recv_data_int[5] = {0, 0, 0, 0, 0};
            float   recv_data_flt[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
            double  recv_data_dbl[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
            
            ASSERT_THAT(recv_data_int, Not(ElementsAreArray(ref_data_int)));
            ASSERT_THAT(recv_data_flt, Not(ElementsAreArray(ref_data_flt)));
            ASSERT_THAT(recv_data_dbl, Not(ElementsAreArray(ref_data_dbl)));
            if (rank == 0) {
                transport.send(ref_data_int, 5, TYPE_INT, 0, i);
                transport.receive(recv_data_int, 5, TYPE_INT, 0, i);
                ASSERT_THAT(recv_data_int, ElementsAreArray(ref_data_int));

                transport.send(ref_data_flt, 5, TYPE_FLOAT, 0, i);
                transport.receive(recv_data_flt, 5, TYPE_FLOAT, 0, i);
                ASSERT_THAT(recv_data_flt, ElementsAreArray(ref_data_flt));

                transport.send(ref_data_dbl, 5, TYPE_DOUBLE, 0, i);
                transport.receive(recv_data_dbl, 5, TYPE_DOUBLE, 0, i);
                ASSERT_THAT(recv_data_dbl, ElementsAreArray(ref_data_dbl));
            } else {
                ASSERT_THAT(recv_data_int, Not(ElementsAreArray(ref_data_int)));
                ASSERT_THAT(recv_data_flt, Not(ElementsAreArray(ref_data_flt)));
                ASSERT_THAT(recv_data_dbl, Not(ElementsAreArray(ref_data_dbl)));
            }
            MPI_Bcast(recv_data_int, 5, MPI_INT, 0, local_comm);
            MPI_Bcast(recv_data_flt, 5, MPI_FLOAT, 0, local_comm);
            MPI_Bcast(recv_data_dbl, 5, MPI_DOUBLE, 0, local_comm);
            ASSERT_THAT(recv_data_int, ElementsAreArray(ref_data_int));
            ASSERT_THAT(recv_data_flt, ElementsAreArray(ref_data_flt));
            ASSERT_THAT(recv_data_dbl, ElementsAreArray(ref_data_dbl));
        }
    }

    // clients
    if (program_id == 1 || program_id == 2) {
        int     recv_data_int[5] = {0, 0, 0, 0, 0};
        float   recv_data_flt[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
        double  recv_data_dbl[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
        
        ASSERT_THAT(recv_data_int, Not(ElementsAreArray(ref_data_int)));
        ASSERT_THAT(recv_data_flt, Not(ElementsAreArray(ref_data_flt)));
        ASSERT_THAT(recv_data_dbl, Not(ElementsAreArray(ref_data_dbl)));
        if (rank == 0) {
            transport.receive(recv_data_int, 5, TYPE_INT, 0, 0);
            ASSERT_THAT(recv_data_int, ElementsAreArray(ref_data_int));
            transport.send(ref_data_int, 5, TYPE_INT, 0, 0);

            transport.receive(recv_data_flt, 5, TYPE_FLOAT, 0, 0);
            ASSERT_THAT(recv_data_flt, ElementsAreArray(ref_data_flt));
            transport.send(ref_data_flt, 5, TYPE_FLOAT, 0, 0);

            transport.receive(recv_data_dbl, 5, TYPE_DOUBLE, 0, 0);
            ASSERT_THAT(recv_data_dbl, ElementsAreArray(ref_data_dbl));
            transport.send(ref_data_dbl, 5, TYPE_DOUBLE, 0, 0);
        } else {
            ASSERT_THAT(recv_data_int, Not(ElementsAreArray(ref_data_int)));
            ASSERT_THAT(recv_data_flt, Not(ElementsAreArray(ref_data_flt)));
            ASSERT_THAT(recv_data_dbl, Not(ElementsAreArray(ref_data_dbl)));
        }
        MPI_Bcast(recv_data_int, 5, MPI_INT, 0, local_comm);
        MPI_Bcast(recv_data_flt, 5, MPI_FLOAT, 0, local_comm);
        MPI_Bcast(recv_data_dbl, 5, MPI_DOUBLE, 0, local_comm);
        ASSERT_THAT(recv_data_int, ElementsAreArray(ref_data_int));
        ASSERT_THAT(recv_data_flt, ElementsAreArray(ref_data_flt));
        ASSERT_THAT(recv_data_dbl, ElementsAreArray(ref_data_dbl));
    }
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    MPI_Init(&argc, &argv);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    TestEventListeners& listeners = UnitTest::GetInstance()->listeners();
    if (world_rank != 0) 
        delete listeners.Release(listeners.default_result_printer());

    auto result = RUN_ALL_TESTS();
    
    MPI_Finalize();
    return result;
}
