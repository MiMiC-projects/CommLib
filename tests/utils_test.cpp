//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "utils.h"

#include <stdlib.h>

#include <cstdlib>
#include <stdexcept>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#if defined(_WIN32) || defined(_WIN64)
#define ADD_ENV_VAR(NAME, VALUE) _putenv(NAME "=" VALUE)
#else
#define ADD_ENV_VAR(NAME, VALUE) setenv(NAME, VALUE, 1)
#endif

using namespace mcl;
using ::testing::ContainsRegex;

void AddEnvVar(std::string var_name, std::string var_value) {
    if (ADD_ENV_VAR(var_name.c_str(), var_value.c_str()) != 0) {
        throw std::runtime_error("Could not set environment variable \""
                                 + var_name + "\" to a value: \"" + var_value + "\"!");
    }
}

TEST(GetEnvVarStr, Positive) {
    AddEnvVar("ENV_VAR", "abcd");
    ASSERT_TRUE(get_env_var_str("ENV_VAR") == "abcd");
}

TEST(GetEnvVarStr, Required) {
    std::string error_msg = "The environment variable, ENV_VAR_NOT_DEFINED is not assigned!";
    ASSERT_DEATH(get_env_var_str("ENV_VAR_NOT_DEFINED", true), error_msg);
}

TEST(GetEnvVarStr, Default) {
    ASSERT_TRUE(get_env_var_str("ENV_VAR_NOT_DEFINED") == std::string());
}

TEST(RepresentsInteger, String) {
    AddEnvVar("ENV_VAR", "abcd");
    ASSERT_FALSE(represents_integer(get_env_var_str("ENV_VAR")));
}


TEST(GetEnvVarInt, StringMinus) {
    AddEnvVar("ENV_VAR", "abc-d");
    std::string error_msg = "The environment variable, ENV_VAR is not an integer!";
    ASSERT_DEATH(get_env_var_int("ENV_VAR"), error_msg); 
}


TEST(GetEnvVarInt, StringOnlyMinus) {
    AddEnvVar("ENV_VAR", "-");
    std::string error_msg = "The environment variable, ENV_VAR is not an integer!";
    ASSERT_DEATH(get_env_var_int("ENV_VAR"), error_msg);
}


TEST(GetEnvVarInt, Float) {
    AddEnvVar("ENV_VAR", "1.238");
    std::string error_msg = "The environment variable, ENV_VAR is not an integer!";
    ASSERT_DEATH(get_env_var_int("ENV_VAR"), error_msg);
}


TEST(GetEnvVarInt, Int) {
    AddEnvVar("ENV_VAR", "1238");
    ASSERT_TRUE(get_env_var_int("ENV_VAR"));
}


TEST(GetEnvVarInt, IntMinus) {
    AddEnvVar("ENV_VAR", "-1238");
    ASSERT_TRUE(get_env_var_int("ENV_VAR"));
}

TEST(GetEnvVarInt, Required) {
    std::string error_msg = "The environment variable, ENV_VAR_NOT_DEFINED is not assigned!";
    ASSERT_DEATH(get_env_var_int("ENV_VAR_NOT_DEFINED"), error_msg);
}

TEST(GetEnvVarInt, RequiredNotInteger) {
    AddEnvVar("ENV_VAR", "1.238");
    std::string error_msg = "The environment variable, ENV_VAR is not an integer!";
    ASSERT_DEATH(get_env_var_int("ENV_VAR"), error_msg);
}

TEST(GetEnvVarInt, Default) {
    ASSERT_TRUE(get_env_var_int("ENV_VAR_NOT_DEFINED", 34) == 34);
}


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    auto result = RUN_ALL_TESTS();
    return result;
}
