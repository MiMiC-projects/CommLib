//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mpi_transport.h"

#include <stdlib.h>

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "mpi.h"

#include "data_types.h"

#if defined(_WIN32) || defined(_WIN64)
#define ADD_ENV_VAR(NAME, VALUE) _putenv(NAME "=" VALUE)
#else
#define ADD_ENV_VAR(NAME, VALUE) setenv(NAME, VALUE, 1)
#endif

using namespace mcl;
using ::testing::TestEventListeners;
using ::testing::UnitTest;
using ::testing::ElementsAreArray;
using ::testing::Not;

class MPITransportTest : public ::testing::Test {
protected:

    static MPI_Comm     world_comm;       // MPI_COMM_WORLD
    static MPI_Comm     mini_world_comm;  // emulates MPI_COMM_WORLD in a run using MPI-type transport
    static int          world_rank;       // rank in world_comm
    static int          rank;             // rank in mini_world_comm
    static MPITransport transport;        // 

    int program_id; // 

    static void SetUpTestSuite() {
       world_comm = MPI_COMM_WORLD;

       int size, program_id_tmp;

       MPI_Comm_size(world_comm, &size);
       if (size != 7)
           throw std::runtime_error("Tests must be run with 7 processes!");

       MPI_Comm_rank(world_comm, &world_rank);
       if (world_rank >= 0 && world_rank < 3) program_id_tmp = 0;
       if (world_rank >= 3 && world_rank < 5) program_id_tmp = 1;
       if (world_rank >= 5 && world_rank < 8) program_id_tmp = 2;
           
       if (ADD_ENV_VAR("MCL_PROG", std::to_string(program_id_tmp).c_str()) != 0)
           throw std::runtime_error("Could not set env var!");

       MPI_Comm_split(world_comm, program_id_tmp, 0, &mini_world_comm);
       MPI_Comm_rank(mini_world_comm, &rank);
    }

    void SetUp() override {
        MPI_Barrier(world_comm);
        transport = MPITransport(mini_world_comm);
        int result = transport.initialize(&mini_world_comm);
        ASSERT_EQ(result, 0);
        program_id = transport.getProgramId();
    }

    void TearDown() override {
        if (rank == 0) {
            if (program_id == 0) {
                transport.destroy(1);
                transport.destroy(2);
            }
            if (program_id == 1 || program_id == 2)
                transport.destroy(0);
        }
    }

};

int           MPITransportTest::world_rank;
int           MPITransportTest::rank;
MPI_Comm      MPITransportTest::world_comm;
MPI_Comm      MPITransportTest::mini_world_comm;
MPITransport  MPITransportTest::transport(mini_world_comm);


TEST_F(MPITransportTest, Initialize) {
    int nPrograms = transport.getNPrograms();
    ASSERT_EQ(nPrograms, 3);

    // server
    if (program_id == 0) {
        ASSERT_FALSE(transport.isClient());
        ASSERT_TRUE((transport.isServer()) == (rank == 0));
    } 
    // clients
    if (program_id == 1 || program_id == 2) {
        ASSERT_FALSE(transport.isServer());
        ASSERT_TRUE((transport.isClient()) == (rank == 0));
    }
}

TEST_F(MPITransportTest, Connection) {
    if (rank == 0) {
        // server
        if (program_id == 0) {
            for (size_t i = 1; i <= 2; i++) {
                int result = transport.acceptConnection(i);
                ASSERT_EQ(result, MPI_SUCCESS);
                transport.closeConnection(i);
            }
        }
        // clients
        if (program_id == 1 || program_id == 2) {
            int result = transport.connect(0);
            ASSERT_EQ(result, MPI_SUCCESS);
            transport.closeConnection(0);
        }
    }
}

TEST_F(MPITransportTest, Probe) {
    constexpr int send_size[2] = {5, 3};

    if (rank == 0) {
        // server
        if (program_id == 0) {
            for (size_t i = 1; i <= 2; i++) {
                transport.acceptConnection(i);
                int size = transport.probe(i, TYPE_INT);
                ASSERT_EQ(size, send_size[i - 1]);
                int* recvBuffer = new int[size];
                transport.receive(recvBuffer, size, TYPE_INT, 0, i);
                transport.closeConnection(i);
                delete[] recvBuffer;
            }
        }
        // clients
        if (program_id == 1 || program_id == 2) {
            std::vector<int> send_data(send_size[program_id - 1]);
            transport.connect(0);
            transport.send(send_data.data(), send_data.size(), TYPE_INT, 0, 0);
            transport.closeConnection(0);
        }
    }
}

TEST_F(MPITransportTest, PingPong) {
    int     ref_data_int[5] = {1, 2, 3, 4, 5};
    float   ref_data_flt[5] = {1.1, 2.2, 3.3, 4.4, 5.5};
    double  ref_data_dbl[5] = {1.1, 2.2, 3.3, 4.4, 5.5};

    // server
    if (program_id == 0) {
        for (size_t i = 1; i <= 2; i++)
            if (rank == 0) transport.acceptConnection(i);

        // integer
        for (size_t i = 1; i <= 2; i++) {
            int    recv_data_int[5] = {0, 0, 0, 0, 0};
            float  recv_data_flt[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
            double recv_data_dbl[5] = {0.0, 0.0, 0.0, 0.0, 0.0};

            ASSERT_THAT(recv_data_int, Not(ElementsAreArray(ref_data_int)));
            ASSERT_THAT(recv_data_flt, Not(ElementsAreArray(ref_data_flt)));
            ASSERT_THAT(recv_data_dbl, Not(ElementsAreArray(ref_data_dbl)));

            if (rank == 0) {
                transport.send(ref_data_int, 5, TYPE_INT, 0, i);
                transport.receive(recv_data_int, 5, TYPE_INT, 0, i);
                ASSERT_THAT(recv_data_int, ElementsAreArray(ref_data_int));

                transport.send(ref_data_flt, 5, TYPE_FLOAT, 0, i);
                transport.receive(recv_data_flt, 5, TYPE_FLOAT, 0, i);
                ASSERT_THAT(recv_data_flt, ElementsAreArray(ref_data_flt));

                transport.send(ref_data_dbl, 5, TYPE_DOUBLE, 0, i);
                transport.receive(recv_data_dbl, 5, TYPE_DOUBLE, 0, i);
                ASSERT_THAT(recv_data_dbl, ElementsAreArray(ref_data_dbl));
            } else {
                ASSERT_THAT(recv_data_int, Not(ElementsAreArray(ref_data_int)));
                ASSERT_THAT(recv_data_flt, Not(ElementsAreArray(ref_data_flt)));
                ASSERT_THAT(recv_data_dbl, Not(ElementsAreArray(ref_data_dbl)));
            }

            MPI_Bcast(recv_data_int, 5, MPI_INT, 0, mini_world_comm);
            MPI_Bcast(recv_data_flt, 5, MPI_FLOAT, 0, mini_world_comm);
            MPI_Bcast(recv_data_dbl, 5, MPI_DOUBLE, 0, mini_world_comm);
            ASSERT_THAT(recv_data_int, ElementsAreArray(ref_data_int));
            ASSERT_THAT(recv_data_flt, ElementsAreArray(ref_data_flt));
            ASSERT_THAT(recv_data_dbl, ElementsAreArray(ref_data_dbl));
        }

        MPI_Barrier(world_comm);
        for (size_t i = 1; i <= 2; i++)
            if (rank == 0) transport.closeConnection(i);
    }

    // clients
    if (program_id == 1 || program_id == 2) {
        if (rank == 0) transport.connect(0);
        
        int     recv_data_int[5] = {0, 0, 0, 0, 0};
        float   recv_data_flt[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
        double  recv_data_dbl[5] = {0.0, 0.0, 0.0, 0.0, 0.0};
        
        ASSERT_THAT(recv_data_int, Not(ElementsAreArray(ref_data_int)));
        ASSERT_THAT(recv_data_flt, Not(ElementsAreArray(ref_data_flt)));
        ASSERT_THAT(recv_data_dbl, Not(ElementsAreArray(ref_data_dbl)));
        if (rank == 0) {
            transport.receive(recv_data_int, 5, TYPE_INT, 0, 0);
            ASSERT_THAT(recv_data_int, ElementsAreArray(ref_data_int));
            transport.send(ref_data_int, 5, TYPE_INT, 0, 0);

            transport.receive(recv_data_flt, 5, TYPE_FLOAT, 0, 0);
            ASSERT_THAT(recv_data_flt, ElementsAreArray(ref_data_flt));
            transport.send(ref_data_flt, 5, TYPE_FLOAT, 0, 0);

            transport.receive(recv_data_dbl, 5, TYPE_DOUBLE, 0, 0);
            ASSERT_THAT(recv_data_dbl, ElementsAreArray(ref_data_dbl));
            transport.send(ref_data_dbl, 5, TYPE_DOUBLE, 0, 0);

        } else {
            ASSERT_THAT(recv_data_int, Not(ElementsAreArray(ref_data_int)));
            ASSERT_THAT(recv_data_flt, Not(ElementsAreArray(ref_data_flt)));
            ASSERT_THAT(recv_data_dbl, Not(ElementsAreArray(ref_data_dbl)));
        }

        MPI_Bcast(recv_data_int, 5, MPI_INT, 0, mini_world_comm);
        MPI_Bcast(recv_data_flt, 5, MPI_FLOAT, 0, mini_world_comm);
        MPI_Bcast(recv_data_dbl, 5, MPI_DOUBLE, 0, mini_world_comm);
        ASSERT_THAT(recv_data_int, ElementsAreArray(ref_data_int));
        ASSERT_THAT(recv_data_flt, ElementsAreArray(ref_data_flt));
        ASSERT_THAT(recv_data_dbl, ElementsAreArray(ref_data_dbl));

        MPI_Barrier(world_comm);
        if (rank == 0) transport.closeConnection(0);
    }
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    MPI_Init(&argc, &argv);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    TestEventListeners& listeners = UnitTest::GetInstance()->listeners();
    if (world_rank != 0) 
        delete listeners.Release(listeners.default_result_printer());
    
    auto result = RUN_ALL_TESTS();

    MPI_Finalize();
    return result;
}
