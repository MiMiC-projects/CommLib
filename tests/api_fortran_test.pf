module api_fortran_test
    
    use, intrinsic :: iso_fortran_env, only: error_unit    
    
    use pFUnit
#ifdef USE_MPIF08
    use mpi_f08
#else
    use mpi
#endif

    use mcl_fortran

    implicit none

    @TestCase
    type, extends(MPITestCase) :: MCLTestCase
#ifdef USE_MPIF08
        type(mpi_comm) :: comm
#else
        integer :: comm
#endif
        integer :: comm_size
        integer :: rank
        integer :: num_programs
        integer :: program_id
    contains
        procedure :: setUp
        procedure :: tearDown
    end type MCLTestCase

contains

subroutine setUp(this)
    class(MCLTestCase), intent(inout) :: this
    integer :: err
    this%comm = MPI_COMM_WORLD
    call mcl_init(this%comm)

    call mpi_comm_rank(this%comm, this%rank, err)
    call mpi_comm_size(this%comm, this%comm_size, err)

    call mcl_get_program_num(this%num_programs)
    call mcl_get_program_id(this%program_id)
    @assertEqual(3, this%num_programs)
end subroutine setUp

subroutine tearDown(this)
    class(MCLTestCase), intent(inout) :: this
    call mcl_destroy()
end subroutine tearDown


@Test(npes=[6])
subroutine mimic_loop_test(this)
    use, intrinsic :: iso_fortran_env, only: real64
    class(MCLTestCase), intent(inout) :: this

    logical :: computed_energy, terminate
    integer, dimension(2) :: num_atoms, ref_num_atoms
    real(kind=real64) :: energy
    real(kind=real64), dimension(2) :: ref_energy
    real(kind=real64), dimension(9) :: coords
    real(kind=real64), dimension(9, 2) :: ref_coords
    integer :: request, client_id
    integer :: i, n_coords, err
  
    ref_num_atoms = [3, 2]
    ref_energy = [0.410_real64, 2.56_real64]
    ref_coords(:, 1) = [ 1.0,  2.0,  3.0,  4.0,  5.0,  6.0,  7.0,  8.0,  9.0]
    ref_coords(: ,2) = [-1.0, -2.0, -3.0, -4.0, -5.0, -6.0,  0.1,  0.1,  0.1]
    
    num_atoms = -1
    energy = 0.1_real64
    coords = 0.1_real64

    @assertEqual(2, this%comm_size)

    ! server
    if (this%program_id == 0) then
        ! MCL_SEND_CLIENT_ID
        do i = 1, 2
            request = MCL_SEND_CLIENT_ID
            call mcl_send(request, 1, MCL_COMMAND, i)
            client_id = -1
            call mcl_receive(client_id, 1, MCL_DATA, i)
            @assertTrue((client_id .eq. i) .eqv. (this%rank .eq. 0))
            call mpi_bcast(client_id, 1, MPI_INT, 0, this%comm, err)
            @assertEqual(i, client_id)
        end do

        ! MCL_SEND_NUM_ATOMS
        do i = 1, 2
            request = MCL_SEND_NUM_ATOMS
            call mcl_send(request, 1, MCL_COMMAND, i)
            call mcl_receive(num_atoms(i), 1, MCL_DATA, i)
            @assertTrue((num_atoms(i) .eq. ref_num_atoms(i)) .eqv. (this%rank .eq. 0))
            call mpi_bcast(num_atoms(i), 1, MPI_INT, 0, this%comm, err)
            @assertEqual(ref_num_atoms(i), num_atoms(i))
        end do
        
        ! MCL_RECV_ATOM_COORDS
        do i = 1, 2
            request = MCL_RECV_ATOM_COORDS
            call mcl_send(request, 1, MCL_COMMAND, i)
            call mcl_send(ref_coords(:,i), 3*num_atoms(i), MCL_DATA, i)
        end do
        
        ! MCL_COMPUTE_ENERGY
        do i = 1, 2
            request = MCL_COMPUTE_ENERGY
            call mcl_send(request, 1, MCL_COMMAND, i)
        end do

        ! MCL_SEND_ENERGY
        do i = 1, 2
            request = MCL_SEND_ENERGY
            call mcl_send(request, 1, MCL_COMMAND, i)
            call mcl_receive(energy, 1, MCL_DATA, i)
            @assertTrue((energy .eq. ref_energy(i)) .eqv. (this%rank .eq. 0))
            call mpi_bcast(energy, 1, MPI_REAL8, 0, this%comm, err)
            @assertEqual(ref_energy(i), energy)
        end do

        ! MCL_EXIT
        request = MCL_EXIT
        call mcl_send(request, 1, MCL_COMMAND, 1)
        call mcl_send(request, 1, MCL_COMMAND, 2)
    end if

    ! clients
    if (this%program_id == 1 .or. this%program_id == 2) then
        computed_energy = .false.
        terminate = .false.
        do while (.not. terminate)
            request = -1
            call mcl_receive(request, 1, MCL_COMMAND, 0)
            call mpi_bcast(request, 1, MPI_INT, 0, this%comm, err)

            if (request == MCL_SEND_CLIENT_ID) then
                call mcl_send(this%program_id, 1, MCL_DATA, 0);
            else if (request == MCL_SEND_NUM_ATOMS) then
                call mcl_send(ref_num_atoms(this%program_id), 1, MCL_DATA, 0);
            else if (request == MCL_RECV_ATOM_COORDS) then
                n_coords = 3*ref_num_atoms(this%program_id)
                call mcl_receive(coords, n_coords, MCL_DATA, 0);
                call mpi_bcast(coords,  n_coords, MPI_REAL8, 0, this%comm, err)
                @assertEqual(ref_coords(:n_coords, this%program_id), coords(:n_coords))
            else if (request == MCL_COMPUTE_ENERGY) then
                computed_energy = .true.
            else if (request == MCL_SEND_ENERGY) then
                @assertTrue(computed_energy)
                call mcl_send(ref_energy(this%program_id), 1, MCL_DATA, 0);
            else if (request == MCL_EXIT) then
                terminate = .true.
            else
                write(error_unit, *) "ABORT: Unknown MCL request!"
                error stop 1
            end if
        end do
    end if
end subroutine mimic_loop_test

@Test(npes=[6])
subroutine mcl_char_test(this)
    class(MCLTestCase), intent(inout) :: this

    character(:), allocatable :: sentence, ref_sentence
    integer :: msg_len, err

    ref_sentence = "But what exactly is a word?"
    msg_len = len(ref_sentence)
    
    if (this%program_id == 0) then
        call mcl_send(ref_sentence, msg_len, MCL_DATA, 1);
    end if

    if (this%program_id == 1) then
        allocate(character(len=msg_len) :: sentence)
        call mcl_receive(sentence, msg_len, MCL_DATA, 0);
        call mpi_bcast(sentence, msg_len, MPI_CHARACTER, 0, this%comm, err)
        @assertEqual(ref_sentence, sentence)
    end if
end subroutine mcl_char_test

@Test(npes=[6])
subroutine mcl_int32_test(this)
    use, intrinsic :: iso_fortran_env, only: int32
    class(MCLTestCase), intent(inout) :: this

    integer(kind=int32) :: buf, buf_1d(6), buf_2d(6,6), buf_3d(6,6,6)
    integer(kind=int32) :: ref, ref_1d(6), ref_2d(6,6), ref_3d(6,6,6)
    integer :: err
    
    ref = 1_int32
    ref_1d = 2_int32
    ref_2d = 3_int32
    ref_3d = 4_int32
    if (this%program_id == 0) then
        call mcl_send(ref, 1, MCL_DATA, 1);
        call mcl_send(ref_1d, size(ref_1d), MCL_DATA, 1);
        call mcl_send(ref_2d, size(ref_2d), MCL_DATA, 1);
        call mcl_send(ref_3d, size(ref_3d), MCL_DATA, 1);
    end if
    
    buf = -1_int32
    buf_1d = -1_int32
    buf_2d = -1_int32
    buf_3d = -1_int32
    if (this%program_id == 1) then
        call mcl_receive(buf, 1, MCL_DATA, 0);
        call mpi_bcast(buf, 1, MPI_INTEGER4, 0, this%comm, err)
        @assertEqual(ref, buf)

        call mcl_receive(buf_1d, size(buf_1d), MCL_DATA, 0);
        call mpi_bcast(buf_1d, size(buf_1d), MPI_INTEGER4, 0, this%comm, err)
        @assertEqual(ref_1d, buf_1d)

        call mcl_receive(buf_2d, size(buf_2d), MCL_DATA, 0);
        call mpi_bcast(buf_2d, size(buf_2d), MPI_INTEGER4, 0, this%comm, err)
        @assertEqual(ref_2d, buf_2d)

        call mcl_receive(buf_3d, size(buf_3d), MCL_DATA, 0);
        call mpi_bcast(buf_3d, size(buf_3d), MPI_INTEGER4, 0, this%comm, err)
        @assertEqual(ref_3d, buf_3d)
    end if
end subroutine mcl_int32_test

@Test(npes=[6])
subroutine mcl_int64_test(this)
    use, intrinsic :: iso_fortran_env, only: int64
    class(MCLTestCase), intent(inout) :: this

    integer(kind=int64) :: buf, buf_1d(6), buf_2d(6,6), buf_3d(6,6,6)
    integer(kind=int64) :: ref, ref_1d(6), ref_2d(6,6), ref_3d(6,6,6)
    integer :: err
    
    ref = 1_int64
    ref_1d = 2_int64
    ref_2d = 3_int64
    ref_3d = 4_int64
    if (this%program_id == 0) then
        call mcl_send(ref, 1, MCL_DATA, 1);
        call mcl_send(ref_1d, size(ref_1d), MCL_DATA, 1);
        call mcl_send(ref_2d, size(ref_2d), MCL_DATA, 1);
        call mcl_send(ref_3d, size(ref_3d), MCL_DATA, 1);
    end if
    
    buf = -1_int64
    buf_1d = -1_int64
    buf_2d = -1_int64
    buf_3d = -1_int64
    if (this%program_id == 1) then
        call mcl_receive(buf, 1, MCL_DATA, 0);
        call mpi_bcast(buf, 1, MPI_INTEGER8, 0, this%comm, err)
        @assertEqual(ref, buf)

        call mcl_receive(buf_1d, size(buf_1d), MCL_DATA, 0);
        call mpi_bcast(buf_1d, size(buf_1d), MPI_INTEGER8, 0, this%comm, err)
        @assertEqual(ref_1d, buf_1d)

        call mcl_receive(buf_2d, size(buf_2d), MCL_DATA, 0);
        call mpi_bcast(buf_2d, size(buf_2d), MPI_INTEGER8, 0, this%comm, err)
        @assertEqual(ref_2d, buf_2d)

        call mcl_receive(buf_3d, size(buf_3d), MCL_DATA, 0);
        call mpi_bcast(buf_3d, size(buf_3d), MPI_INTEGER8, 0, this%comm, err)
        @assertEqual(ref_3d, buf_3d)
    end if
end subroutine mcl_int64_test

@Test(npes=[6])
subroutine mcl_real32_test(this)
    use, intrinsic :: iso_fortran_env, only: real32
    class(MCLTestCase), intent(inout) :: this

    real(kind=real32) :: buf, buf_1d(6), buf_2d(6,6), buf_3d(6,6,6)
    real(kind=real32) :: ref, ref_1d(6), ref_2d(6,6), ref_3d(6,6,6)
    integer :: err

    ref = 1_real32
    ref_1d = 2_real32
    ref_2d = 3_real32
    ref_3d = 4_real32
    if (this%program_id == 0) then
        call mcl_send(ref, 1, MCL_DATA, 1);
        call mcl_send(ref_1d, size(ref_1d), MCL_DATA, 1);
        call mcl_send(ref_2d, size(ref_2d), MCL_DATA, 1);
        call mcl_send(ref_3d, size(ref_3d), MCL_DATA, 1);
    end if
    
    buf = -1_real32
    buf_1d = -1_real32
    buf_2d = -1_real32
    buf_3d = -1_real32
    if (this%program_id == 1) then
        call mcl_receive(buf, 1, MCL_DATA, 0);
        call mpi_bcast(buf, 1, MPI_REAL4, 0, this%comm, err)
        @assertEqual(ref, buf)

        call mcl_receive(buf_1d, size(buf_1d), MCL_DATA, 0);
        call mpi_bcast(buf_1d, size(buf_1d), MPI_REAL4, 0, this%comm, err)
        @assertEqual(ref_1d, buf_1d)

        call mcl_receive(buf_2d, size(buf_2d), MCL_DATA, 0);
        call mpi_bcast(buf_2d, size(buf_2d), MPI_REAL4, 0, this%comm, err)
        @assertEqual(ref_2d, buf_2d)

        call mcl_receive(buf_3d, size(buf_3d), MCL_DATA, 0);
        call mpi_bcast(buf_3d, size(buf_3d), MPI_REAL4, 0, this%comm, err)
        @assertEqual(ref_3d, buf_3d)
    end if
end subroutine mcl_real32_test

@Test(npes=[6])
subroutine mcl_real64_test(this)
    use, intrinsic :: iso_fortran_env, only: real64
    class(MCLTestCase), intent(inout) :: this
    
    real(kind=real64) :: buf, buf_1d(6), buf_2d(6,6), buf_3d(6,6,6)
    real(kind=real64) :: ref, ref_1d(6), ref_2d(6,6), ref_3d(6,6,6)
    integer :: err
    
    ref = 1_real64
    ref_1d = 2_real64
    ref_2d = 3_real64
    ref_3d = 4_real64
    if (this%program_id == 0) then
        call mcl_send(ref, 1, MCL_DATA, 1);
        call mcl_send(ref_1d, size(ref_1d), MCL_DATA, 1);
        call mcl_send(ref_2d, size(ref_2d), MCL_DATA, 1);
        call mcl_send(ref_3d, size(ref_3d), MCL_DATA, 1);
    end if
    
    buf = -1_real64
    buf_1d = -1_real64
    buf_2d = -1_real64
    buf_3d = -1_real64
    if (this%program_id == 1) then
        call mcl_receive(buf, 1, MCL_DATA, 0);
        call mpi_bcast(buf, 1, MPI_REAL8, 0, this%comm, err)
        @assertEqual(ref, buf)

        call mcl_receive(buf_1d, size(buf_1d), MCL_DATA, 0);
        call mpi_bcast(buf_1d, size(buf_1d), MPI_REAL8, 0, this%comm, err)
        @assertEqual(ref_1d, buf_1d)

        call mcl_receive(buf_2d, size(buf_2d), MCL_DATA, 0);
        call mpi_bcast(buf_2d, size(buf_2d), MPI_REAL8, 0, this%comm, err)
        @assertEqual(ref_2d, buf_2d)

        call mcl_receive(buf_3d, size(buf_3d), MCL_DATA, 0);
        call mpi_bcast(buf_3d, size(buf_3d), MPI_REAL8, 0, this%comm, err)
        @assertEqual(ref_3d, buf_3d)
    end if
end subroutine mcl_real64_test

@Test(npes=[6])
subroutine mcl_get_request_name_test(this)
    class(MCLTestCase), intent(inout) :: this
    
    @assertEqual("MCL_DATA", mcl_request_name(MCL_DATA))
    @assertEqual("MCL_SEND_CLIENT_ID", mcl_request_name(MCL_SEND_CLIENT_ID))
    @assertEqual("MCL_SEND_ENERGY", mcl_request_name(MCL_SEND_ENERGY))
    @assertEqual("MCL_EXIT", mcl_request_name(MCL_EXIT))
end subroutine mcl_get_request_name_test

end module api_fortran_test
