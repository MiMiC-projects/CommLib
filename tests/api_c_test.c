//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mcl_c.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "mpi.h"


void ASSERT_TRUE(bool a) { if (!a) abort(); }
void ASSERT_EQ_ARRAYS(double * a, double * b, int len) {
    for (int i = 0; i < len; i++)
        ASSERT_TRUE(a[i] == b[i]);
}

int APICTest() {
    MPI_Comm localComm = MPI_COMM_WORLD;
    MCL_init(&localComm);
    
    int commRank;
    MPI_Comm_rank(localComm, &commRank); 
    int commSize;
    MPI_Comm_size(localComm, &commSize);

    int programId = MCL_Get_program_id();
    int numPrograms = MCL_Get_program_num();
    ASSERT_TRUE(numPrograms == 3);

    double ref_energy[2] = {0.410, 2.56};
    int    ref_num_atoms[2] = {3, 2};
    double ref_coords[2][9] = {{ 1.0,  2.0,  3.0,  4.0,  5.0,  6.0,  7.0,  8.0,  9.0 },
                               {-1.0, -2.0, -3.0, -4.0, -5.0, -6.0,  0.0,  0.0,  0.0 }};

    // server
    if (programId == 0) {
        ASSERT_TRUE(commSize == 3);
        int request;

        // MCL_SEND_CLIENT_ID
        for (int i = 1; i <= 2; i++) {
            request = MCL_SEND_CLIENT_ID;
            MCL_send(&request, 1, MCL_INT, MCL_COMMAND, i);
            int clientId = -1;
            MCL_receive(&clientId, 1, MCL_INT, MCL_DATA, i);
            ASSERT_TRUE((clientId == i) == (commRank == 0));
            MPI_Bcast(&clientId, 1, MPI_INT, 0, localComm);
            ASSERT_TRUE(clientId == i);
        }

        int num_atoms[2] = {-1, -1};
        // MCL_SEND_NUM_ATOMS
        for (int i = 1; i <= 2; i++) {
            request = MCL_SEND_NUM_ATOMS;
            MCL_send(&request, 1, MCL_INT, MCL_COMMAND, i);
            MCL_receive(&(num_atoms[i-1]), 1, MCL_INT, MCL_DATA, i);
            ASSERT_TRUE((num_atoms[i-1] == ref_num_atoms[i-1]) == (commRank == 0));
            MPI_Bcast(&(num_atoms[i-1]), 1, MPI_INT, 0, localComm);
            ASSERT_TRUE(num_atoms[i-1] == ref_num_atoms[i-1]);
        }

        // MCL_RECV_ATOM_COORDS
        for (int i = 1; i <= 2; i++) {
            request = MCL_RECV_ATOM_COORDS;
            MCL_send(&request, 1, MCL_INT, MCL_COMMAND, i);
            MCL_send(ref_coords[i-1], 3*num_atoms[i-1], MCL_DOUBLE, MCL_DATA, i);
        }

        // MCL_COMPUTE_ENERGY
        for (int i = 1; i <= 2; i++) {
            request = MCL_COMPUTE_ENERGY;
            MCL_send(&request, 1, MCL_INT, MCL_COMMAND, i);
        }

        // MCL_SEND_ENERGY
        for (size_t i = 1; i <= 2; i++) {
            request = MCL_SEND_ENERGY;
            MCL_send(&request, 1, MCL_INT, MCL_COMMAND, i);
            double energy = 0.0;
            MCL_receive(&energy, 1, MCL_DOUBLE, MCL_DATA, i);
            ASSERT_TRUE((energy == ref_energy[i-1]) == (commRank == 0));
            MPI_Bcast(&energy, 1, MPI_DOUBLE, 0, localComm);
            ASSERT_TRUE(energy == ref_energy[i-1]);
        }

        // MCL_EXIT
        request = MCL_EXIT;
        MCL_send(&request, 1, MCL_INT, MCL_COMMAND, 1);
        MCL_send(&request, 1, MCL_INT, MCL_COMMAND, 2);
    }

    // clients
    if (programId == 1 || programId == 2) {
        ASSERT_TRUE(commSize == 2);
        
        int num_atoms = ref_num_atoms[programId-1];

        bool computed_energy = false;
        bool terminate = false;
        while (!terminate) {
            int request = -1;
            MCL_receive(&request, 1, MCL_INT, MCL_COMMAND, 0);
            MPI_Bcast(&request, 1, MPI_INT, 0, localComm);

            if (request == MCL_SEND_CLIENT_ID) {
                MCL_send(&programId, 1, MCL_INT, MCL_DATA, 0);
            } else if (request == MCL_SEND_NUM_ATOMS) {
                MCL_send(&num_atoms, 1, MCL_INT, MCL_DATA, 0);
            } else if (request == MCL_RECV_ATOM_COORDS) {
                double coords[9] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
                MCL_receive(coords, 3*num_atoms, MCL_DOUBLE, MCL_DATA, 0);
                MPI_Bcast(coords, 3*num_atoms, MPI_DOUBLE, 0, localComm);
                ASSERT_EQ_ARRAYS(coords, ref_coords[programId-1], 3*num_atoms);
            } else if (request == MCL_COMPUTE_ENERGY) {
                computed_energy = true;
            } else if (request == MCL_SEND_ENERGY) {
                ASSERT_TRUE(computed_energy);
                double energy = ref_energy[programId - 1];
                MCL_send(&energy, 1, MCL_DOUBLE, MCL_DATA, 0);
            } else if (request == MCL_EXIT) {
                terminate = true;
            } else {
                fprintf(stderr, "ABORT: Unknown MCL request!");
                abort();
            }
        }
    }

    MCL_destroy();
    return 0;
}

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    
    int worldRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &worldRank);

    int result = APICTest();
    
    MPI_Finalize();
    return result;
}
