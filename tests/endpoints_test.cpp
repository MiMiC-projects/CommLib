//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "client.h"
#include "server.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include "data_types.h"

using namespace mcl;
using ::testing::Exactly;
using ::testing::_;
using ::testing::Return;
using ::testing::ContainsRegex;
using ::testing::Invoke;

class MockTransport : public Transport {
public:
    MockTransport() : Transport() {}

    ~MockTransport() override = default;

    MOCK_METHOD(int,  initialize,       (void *args));
    MOCK_METHOD(int,  connect,          (int id));
    MOCK_METHOD(int,  acceptConnection, (int id));
    MOCK_METHOD(void, closeConnection,  (int id));
    MOCK_METHOD(int,  probe,            (int id, int data_type));
    MOCK_METHOD(void, destroy,          (int id));
    MOCK_METHOD(void, send,             (void* data, int count, int data_type, int tag, int dest));
    MOCK_METHOD(void, receive,          (void* data_holder, int count, int data_type, int tag, int source));
    MOCK_METHOD(bool, isServer,         (), (const));
    MOCK_METHOD(bool, isClient,         (), (const));
    MOCK_METHOD(int,  getNPrograms,     (), (const));
    MOCK_METHOD(int,  getProgramId,     (), (const));
};

// receive the id value in data for protocol.receive()
void ReceiveId(void* data, int count, int data_type, int tag, int source) {
    *((int*) data) = source;
}
std::function<void(void*, int, int, int, int)> Receive = ReceiveId;

class ServerTest : public ::testing::Test {
  protected:
    std::shared_ptr<MockTransport> protocol;
    Server* server;
    size_t num_program;

    void SetUp() override {
        num_program = 5;
        protocol = std::make_shared<MockTransport>();
        
        int client_id = 1;
        EXPECT_CALL(*protocol, getNPrograms()).Times(Exactly(1)).WillOnce(Return(num_program));
        for (size_t i = 1; i < num_program; i++) {
            EXPECT_CALL(*protocol, acceptConnection(i)).Times(Exactly(1));
            EXPECT_CALL(*protocol, receive(_, 1, TYPE_INT, _, i)).Times(Exactly(1))
                                    .WillOnce([&](void* data, int count, int data_type, int tag, int source) {
                                                *((int*) data) = client_id++;
                                              }
                                             );
        }
        server = new Server(protocol);
    }

    void TearDown() override { delete server; }
};

TEST_F(ServerTest, Initialize) {
    ASSERT_EQ(server->getId(), 0);
    ASSERT_EQ(server->getClient_list().size(), num_program - 1);
    for (size_t i = 0; i < server->getClient_list().size(); ++i)
        ASSERT_EQ(i + 1, server->getClient_list()[i]->getId());
}


TEST_F(ServerTest, Disconnect) {
    for (size_t i = 1; i < num_program; i++) {
        EXPECT_CALL(*protocol, closeConnection(i + 1)).Times(Exactly(1));
        server->disconnect(i + 1);
    }
}

TEST_F(ServerTest, Destroy) {
    for (size_t i = 1; i < num_program; i++) {
        EXPECT_CALL(*protocol, closeConnection(i)).Times(Exactly(1));
        EXPECT_CALL(*protocol, destroy(i)).Times(Exactly(1));
    }
    server->destroy();
}

TEST_F(ServerTest, FailedAccept) {
    ASSERT_DEATH(
        [this] {
          EXPECT_CALL(*protocol, getNPrograms()).WillOnce(Return(2));
          EXPECT_CALL(*protocol, acceptConnection(1)).WillOnce(Return(1));
          Server server_fail(protocol);
        }(), ContainsRegex("Server::Init : Connection was not successful"));
}

TEST_F(ServerTest, FailedClientId) {
    ASSERT_DEATH(
        [this] {
          EXPECT_CALL(*protocol, getNPrograms()).WillOnce(Return(2));
          EXPECT_CALL(*protocol, acceptConnection(1)).WillOnce(Return(0));
          EXPECT_CALL(*protocol, receive(_, 1, TYPE_INT, _, 1)).Times(Exactly(1)).WillOnce(
            [&](void* data, int count, int data_type, int tag, int source) { *((int*) data) = 5; }
          );
          Server server_fail(protocol);
        }(), ContainsRegex("Server::Init : Received client ID does not match its"));
}


class EndpointTest : public ServerTest {
  protected:
    int fail_client = 5;
};

TEST_F(EndpointTest, Send) {
    int data;
    for (size_t i = 1; i < num_program; i++) {
        EXPECT_CALL(*protocol, send(_, 1, TYPE_INT, _, i)).Times(Exactly(1));
        server->send(&data, 1, i, TYPE_INT, 0);
    }
}

TEST_F(EndpointTest, SendFail) {
    int data;
    std::string error_msg = "Send: Unknown destination is chosen!";
    ASSERT_DEATH(server->send(&data, 1, fail_client, TYPE_INT, 0), error_msg);
}

TEST_F(EndpointTest, Receive) {
    for (size_t i = 1; i < num_program; i++) {
        int data = 0;
        EXPECT_CALL(*protocol, receive(_, 1, TYPE_INT, _, i)).Times(Exactly(1)).WillOnce(Receive);
        server->receive(&data, 1, i, TYPE_INT, 0);
        ASSERT_TRUE(data == (int) i);
    }
}

TEST_F(EndpointTest, ReceiveFail) {
    int data;
    std::string error_msg = "Receive: Unknown source is chosen!";
    ASSERT_DEATH(server->receive(&data, 1, fail_client, TYPE_INT, 0), ContainsRegex(error_msg));
}


class ClientTest : public ::testing::Test {
  protected:
    std::shared_ptr<MockTransport> protocol;
    Client * client;
    size_t client_id;

    void SetUp() override {
        client_id = 1;
        protocol = std::make_shared<MockTransport>();

        EXPECT_CALL(*protocol, getProgramId()).Times(Exactly(1)).WillRepeatedly(Return(client_id));
        EXPECT_CALL(*protocol, connect(0)).Times(Exactly(1));
        EXPECT_CALL(*protocol, send(_, 1, TYPE_INT, _, 0)).Times(Exactly(1));
        client = new Client(protocol);
    }

    void TearDown() override { delete client; }
};

TEST_F(ClientTest, Initialize) {
    ASSERT_EQ(1, client->getId());
    ASSERT_EQ(1u, client->getClient_list().size());
    ASSERT_EQ(0, client->getClient_list()[0]->getId());
}

TEST_F(ClientTest, Disconnect) {
    EXPECT_CALL(*protocol, closeConnection(0)).Times(Exactly(1));
    client->disconnect(0);
}

TEST_F(ClientTest, Destroy) {
    EXPECT_CALL(*protocol, closeConnection(0)).Times(Exactly(1));
    client->destroy();
}

TEST_F(ClientTest, Probe) {
    EXPECT_CALL(*protocol, probe(0, TYPE_INT)).Times(Exactly(1));
    client->probe(0, TYPE_INT);
}


int main(int argc, char** argv) {
    ::testing::InitGoogleMock(&argc, argv);
    auto result = RUN_ALL_TESTS();
    return result;
}
