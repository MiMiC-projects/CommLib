# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/)
and this project adheres to [Semantic Versioning](https://semver.org/).

## [Unreleased]
### Added
- Function that translates request codes to request names
- Support for mpi_f08 module
- API additions: `MCL_get_program_num`, `MCL_get_program_id` and `MCL_request_name`

### Fixed
- Bug in Fortran API procedure for sending 32-bit real 2D array

### Changed
- All commands moved to REQUESTS and the request source files are automatically generated during compilation
- More robust MPMD communication mechanism (now requires MPI rank 0 call server initialization)
- Client identification based on working directories (in MPMD determined automatically, in MPI through environment variables)

### Removed
- API removal: `MCL_handshake` function (functionality merged with `MCL_init`)

## [2.0.2] - 2022-12-31
### Fixed
- Installation of Fortran modules when using Cray Compiling Environment

## [2.0.1] - 2022-10-29
### Added
- Check that environment variables are integer
- CONTRIBUTORS file

### Changed
- Minimum C++ standard changed to C++17

## [2.0.0] - 2021-06-30
### Added
- New communication mechanism through MPI MPMD
- Fortran API for library functions
- Concise customizable error handling
- User documentation

### Changed
- Switched to C++14 in core modules
- Updated CMake configuration to follow modern standards

### Removed
- Downloading of external CMake projects

## [1.0.0] - 2017-05-31
### Changed
- Code cleanup
- Increased test coverage

## [1.0.0] - 2017-05-19
### Added
- First public release of MCL
