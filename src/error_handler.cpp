//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "error_handler.h"

#include <iostream>
#include <sstream>

#include "mcl_config.h"

namespace mcl {

static constexpr char INDENT[]   = " ";
static constexpr int  LINE_WIDTH = 85;

static std::string print_header(const char* file, int line) {
    std::stringstream stream{};

    stream << INDENT << "Program:      " << PROJECT_NAME << " v" << PROJECT_VER << std::endl;
    if (file != nullptr) {
        stream << INDENT << "Source file:  " << file << std::endl;
        stream << INDENT << "Line:         " << line << std::endl;
    }

    return stream.str();
}

static std::string print_body(const std::string &msg) {
    std::stringstream stream{};

    stream << INDENT << msg << std::endl;

    return stream.str();
}

static void default_error_handler(const int type, const std::string &msg, const char* file, int line)
{
    std::cerr << std::string(LINE_WIDTH, '~') << std::endl;
    std::cerr << print_header(file, line);
    std::cerr << std::endl;
    std::cerr << print_body(msg);
    std::cerr << std::string(LINE_WIDTH, '~') << std::endl;

    if (type == ERROR_FATAL)
        std::abort();
}

static mcl_error_handler errorHandler = default_error_handler;

void set_error_handler(mcl_error_handler handler) {
    errorHandler = handler;
}

void handle_error(const int type, const std::string& msg, const char* file, int line) {
    errorHandler(type, msg, file, line);
}

} // namespace mcl
