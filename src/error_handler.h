//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_ERRORHANDLER_H_
#define MCL_ERRORHANDLER_H_

#include <string>

namespace mcl{

#define EARGS __FILE__, __LINE__

static constexpr int ERROR_WARNING = 1;
static constexpr int ERROR_FATAL = 2;

typedef void (*mcl_error_handler)(const int type, const std::string& msg, const char* file, int line);

void set_error_handler(mcl_error_handler handler);

void handle_error(int type, const std::string& msg, const char* file, int line);

}

#endif // MCL_ERRORHANDLER_H_
