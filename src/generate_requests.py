#  MCL: MiMiC Communication Library
#  Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
#
#  This file is part of MCL.
#
#  MCL is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
#
#  MCL is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys


def get_license(comment_char='//'):
    """Return copyright and license header."""
    return """\
{0}    MCL: MiMiC Communication Library
{0}    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
{0}
{0}    This file is part of MCL.
{0}
{0}    MCL is free software: you can redistribute it and/or modify
{0}    it under the terms of the GNU Lesser General Public License as
{0}    published by the Free Software Foundation, either version 3 of
{0}    the License, or (at your option) any later version.
{0}
{0}    MCL is distributed in the hope that it will be useful, but
{0}    WITHOUT ANY WARRANTY; without even the implied warranty of
{0}    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
{0}    GNU Lesser General Public License for more details.
{0}
{0}    You should have received a copy of the GNU Lesser General Public License
{0}    along with this program.  If not, see <http://www.gnu.org/licenses/>.
\n""".format(comment_char)


def parse_requests(file):
    """Parse REQUESTS file containing requests and their respective values."""
    with open(f'{file}', 'r') as requests_file:
        requests_lines = requests_file.readlines()
    requests = []
    request_id = 6424
    for line in requests_lines:
        line = line.split()
        if line == [] or '***' in line:
            continue
        elif line[0][:3] == 'MCL':
            request = line[0]
            comment = ' '.join(line[2:]) if '|' in line else ''
            requests.append((request_id, request, comment))
            request_id += 1
    return requests


def check_requests(requests):
    """Check the validity of the REQUESTS file."""
    request_names = list(list(zip(*requests))[1])
    duplicate_reqs = [req for req in set(request_names) if request_names.count(req) > 1]
    if len(duplicate_reqs) > 0:
        error_message = 'Failed to generate Request source codes, because of the following ' + \
                        'duplicate requests in the "REQUESTS" file:\n' + \
                        '\n'.join(f'{req}' for req in duplicate_reqs)
        raise Exception(error_message)
    forbidden_reqs = {'MCL_CHAR', 'MCL_INT', 'MCL_LONG_INT', 'MCL_FLOAT', 'MCL_DOUBLE'}
    forbidden_reqs = forbidden_reqs & set(request_names)
    if len(forbidden_reqs) > 0:
        error_message = 'Failed to generate Request source codes, because of the following ' + \
                        'forbidden requests names used in the "REQUESTS" file:\n' + \
                        '\n'.join(f'{req}' for req in forbidden_reqs)
        raise Exception(error_message)


def write_header_requests(requests):
    """Write C/C++ requests header file."""
    source = get_license()
    source += '#ifndef MCL_MCLREQUESTS_H_\n'
    source += '#define MCL_MCLREQUESTS_H_\n\n'
    for request in requests:
        req_id, req, comment = request
        source += f'/// {comment}\n'
        source += f'const int {req} = {req_id};\n\n'
    source += '/// Returns the name associated with the given request value\n'
    source += f'#ifdef __cplusplus\n'
    source += f'extern "C"\n'
    source += f'#endif\n'
    source += 'char* MCL_request_name(int request);\n\n'
    source += '#endif // MCL_MCLREQUESTS_H_'
    with open(f'mcl_requests.h', 'w') as source_file:
        source_file.write(source)


def write_cpp_requests(requests):
    """Write C/C++ requests file."""
    source = get_license()
    source += '#include "mcl_requests.h"\n\n'
    source += '// Returns the name associated with the given request value\n'
    source += 'char* MCL_request_name(int request) {\n'
    source += '    char* request_name = new char;\n\n'
    source += '    switch (request) {\n'
    for request in requests:
        _, req, _ = request
        source += f'        case {req}:\n'
        source += f'            request_name = (char*) "{req}";\n'
        source += '            break;\n'
    source += '        default:\n'
    source += '            request_name = (char*) "Unknown request";\n'
    source += '    }\n\n'
    source += '  return request_name;\n};\n'
    with open(f'mcl_requests.cpp', 'w') as source_file:
        source_file.write(source)


def write_cpp_request_name_test(requests):
    """A test for MCL_request_name() function."""
    source = get_license()
    source += '#include <gmock/gmock.h>\n'
    source += '#include <gtest/gtest.h>\n\n'
    source += '#include "mcl_requests.h"\n\n'
    source += 'using ::testing::StrEq;\n\n'
    source += 'TEST(Requests, RequestName) {\n'
    for request in requests:
        req_id, req, _ = request
        source += f'    ASSERT_THAT(MCL_request_name({req}), StrEq("{req}"));\n'
    source += '    ASSERT_THAT(MCL_request_name(-1), StrEq("Unknown request"));\n'
    source += '}\n\n'
    source += 'int main(int argc, char** argv) {\n'
    source += '    ::testing::InitGoogleTest(&argc, argv);\n'
    source += '    auto result = RUN_ALL_TESTS();\n'
    source += '    return result;\n'
    source += '}'
    with open(f'requests_test.cpp', 'w') as source_file:
        source_file.write(source)


def write_fortran_requests(requests):
    """Write Fortran requests module file."""
    source = get_license('!')
    source += '\n'
    for request in requests:
        req_id, req, comment = request
        source += f'    !> {comment}\n'
        source += f'    integer(kind=c_int), parameter :: {req} = {req_id}\n\n'
    with open(f'mcl_requests.inc', 'w') as source_file:
        source_file.write(source)
    

if __name__ == '__main__':
    requests_file = sys.argv[1]
    which_code = sys.argv[2]
    requests = parse_requests(requests_file)
    check_requests(requests)
    if (which_code == 'source'):
        write_fortran_requests(requests)
        write_header_requests(requests)
        write_cpp_requests(requests)
    if (which_code == 'test'):
        write_cpp_request_name_test(requests)
