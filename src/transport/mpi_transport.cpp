//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mpi_transport.h"

#include <algorithm>
#include <cmath>
#include <filesystem>
#include <fstream>
#include <thread>

#include "data_types.h"
#include "utils.h"

namespace mcl {

void MPITransport::setTimeout() {
    int timeout = get_env_var_int(TIMEOUT_KEY, TIMEOUT_DEFAULT);
    
    if (std::signbit(timeout)) {
        handle_error(ERROR_FATAL, "The requested environment variable, \"TIMEOUT_KEY\" "
                                  "has a negative value!", EARGS);
    }
    this->timeout = timeout;
}

int MPITransport::initialize(void *args) {

    static constexpr auto PROGRAM_NUMBER = "MCL_PROG";
    programId = get_env_var_int(PROGRAM_NUMBER);

    static constexpr auto PORT_DIRECTORY = "MCL_PORT_DIR";
    portDir = get_env_var_str(PORT_DIRECTORY, true);

    int localRank;
    MPI_Comm_rank(worldComm, &localRank);

    std::string portFile = portDir + "/" + PORT_FILENAME + "_";
    std::string nProgFile = portDir + "/" + NPROG_FILENAME;
    std::string writeFile = portDir + "/" + ".MCL_write_portfiles";

    rankIsServer = false;
    rankIsClient = false;

    if (localRank == 0) {
        if (programId == 0) { 
            setTimeout();

            // remove potentially undeleted files from previous simulations
            std::filesystem::remove(nProgFile);
            for (auto &file : std::filesystem::directory_iterator(portDir)) {
                std::string fileName = file.path().filename().string();
                if (fileName.find(PORT_FILENAME) == 0)
                    std::filesystem::remove(file);
            }
            
            // create a writeFile to instruct clients to create port files 
            std::this_thread::sleep_for(std::chrono::milliseconds(250));
            std::fstream f(writeFile, std::fstream::out);
            if (f.is_open()) f.close();

            // count the total number of portFiles, i.e., the number of programs
            nPrograms = -1;
            int clientCount = 0;
            while (nPrograms != clientCount) {
                nPrograms = clientCount;
                clientCount = 0;
                std::this_thread::sleep_for(std::chrono::milliseconds(250));
                for (auto &file : std::filesystem::directory_iterator(portDir)) {
                    std::string fileName = file.path().filename().string();
                    if (fileName.find(PORT_FILENAME) == 0)
                        clientCount++;
                }
            }
            nPrograms++;
            
            // write the total number of programs to a file to share with clients
            f.open(nProgFile, std::fstream::out);
            if (f.is_open()) {
                f << std::to_string(nPrograms);
                f.close();
            }

            // open ports for client programs and store them in the portFiles
            ports.reserve(nPrograms);
            interComms.reserve(nPrograms);
            ports.push_back(Port{});
            interComms.push_back(selfComm);
            for (int i = 1; i < nPrograms; ++i) {
                Port port{};
                MPI_Open_port(MPI_INFO_NULL, port.name);
                ports.push_back(port);
                interComms.push_back(MPI_COMM_NULL);
                std::string portFileTmp = portFile + std::to_string(i);
                f.open(portFileTmp, std::fstream::out);
                if (f.is_open()) {
                    f << port.name;
                    f.close();
                }
            }
            
            // remove the writeFile to instruct clients to read the port files
            std::filesystem::remove(writeFile);
            
            // check that the portFiles were deleted and remove the file with num_programs
            bool deleted = false;
            while (!deleted) {
                deleted = true;
                for (auto &file : std::filesystem::directory_iterator(portDir)) {
                    std::string fileName = file.path().filename().string();
                    if (fileName.find(PORT_FILENAME) == 0)
                        deleted = false;
                }
            }
            std::filesystem::remove(nProgFile);
            
            rankIsServer = true;
        } else {
            setTimeout();
            portFile += std::to_string(programId);

            // wait for the signal from server (writeFile created)
            while (!std::filesystem::exists(writeFile));

            // write the "portFile"
            std::fstream f(portFile, std::fstream::out);
            if (f.is_open()) f.close();
            
            // wait for the signal from server (writeFile deleted)
            while (std::filesystem::exists(writeFile));
            
            // get the total number of programs from the server
            f.open(nProgFile, std::fstream::in);
            if (f.good()) {
                f >> nPrograms;
                f.close();
            }

            ports.reserve(1);
            interComms.reserve(1);
            interComms.push_back(MPI_Comm{});

            // read the port name from the respective portFile
            Port port{};
            f.open(portFile, std::fstream::in);
            if (f.good()) {
                f >> port.name;
                f.close();
            }
            ports.push_back(port);
            std::filesystem::remove(portFile);

            rankIsClient = true;
        }
    }
    MPI_Bcast(&nPrograms, 1, MPI_INT, 0, worldComm);

    return 0;
}

int MPITransport::connect(int id) {
    auto result = std::async(std::launch::async,
                             MPI_Comm_connect,
                             ports[id].name, MPI_INFO_NULL, 0, selfComm, &interComms[id]);
    checkTimeout(result, ports[id]);
    return result.get();
}

int MPITransport::acceptConnection(int id) {
    auto result = std::async(std::launch::async,
                             MPI_Comm_accept,
                             ports[id].name, MPI_INFO_NULL, 0, selfComm, &interComms[id]);
    checkTimeout(result, ports[id]);
    return result.get();
}

void MPITransport::closeConnection(int id) {
    MPI_Comm_disconnect(&interComms[id]);
}

void MPITransport::send(void *data, int count, int data_type, int tag, int dest) {
    MPI_Ssend(data, count, pick_mpi_type(data_type), 0, tag, interComms[dest]);
}

void MPITransport::receive(void *data, int count, int data_type, int tag, int source) {
    MPI_Status status;
    MPI_Recv(data, count, pick_mpi_type(data_type), 0, tag, interComms[source], &status);
}

int MPITransport::probe(int id, int data_type) {
    int size = 0;
    MPI_Status status{};
    MPI_Probe(0, MPI_ANY_TAG, interComms[id], &status);
    MPI_Get_count(&status, pick_mpi_type(data_type), &size);
    return size;
}

void MPITransport::destroy(int id) {
    MPI_Close_port(ports[id].name);
}

MPI_Datatype MPITransport::pick_mpi_type(int data_type) {
    MPI_Datatype mpi_type;
    if (data_type == TYPE_CHAR) {
        mpi_type = MPI_CHAR;
    } else if (data_type == TYPE_INT) {
        mpi_type = MPI_INT;
    } else if (data_type == TYPE_LONG_INT) {
        mpi_type = MPI_LONG;
    } else if (data_type == TYPE_FLOAT) {
        mpi_type = MPI_FLOAT;
    } else if (data_type == TYPE_DOUBLE) {
        mpi_type = MPI_DOUBLE;
    } else {
        mpi_type = MPI_CHAR;
        handle_error(ERROR_WARNING, "MPITransport: Unknown type 'TYPE_FIX' chosen! "
                                    "Transferring the message as 'TYPE_CHAR'!", EARGS);
    }
    return mpi_type;
}

} // namespace mcl
