//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_TRANSPORT_MPMDTRANSPORT_H_
#define MCL_TRANSPORT_MPMDTRANSPORT_H_

#include <csignal>
#include <future>
#include <iostream>

#include "mpi.h"

#include "transport.h"

namespace mcl {

class MPMDTransport : public Transport {

private:

    /// Global world communicator
    MPI_Comm worldComm;

    /// Local communicator handling communication within a single entity
    MPI_Comm localComm;

    /// Array of intercommunicators that will handle connections
    std::vector<MPI_Comm> interComms;

    static MPI_Datatype pick_mpi_type(int data_type);

public:

    explicit MPMDTransport(MPI_Comm comm) : Transport(), worldComm(comm), localComm(comm) { }

    ~MPMDTransport() override = default;

    int initialize(void *args) override;

    void send(void *data, int count, int data_type, int tag, int dest) override;

    void receive(void *data, int count, int data_type, int tag, int source) override;

    int probe(int id, int data_type) override;

    // No need to do anything here
    int connect(int id) override { return 0; };

    // No need to do anything here
    int acceptConnection(int id) override { return 0; };

    // No need to do anything here
    void closeConnection(int id) override { };

    // No need to do anything here
    void destroy(int id) override { };

};

}

#endif // MCL_TRANSPORT_MPMDTRANSPORT_H_
