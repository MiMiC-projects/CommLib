//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_TRANSPORT_TRANSPORT_H_
#define MCL_TRANSPORT_TRANSPORT_H_

#include <string>
#include <vector>

namespace mcl {

/// Abstract class of transport layer
class Transport {
    
protected:

    /// Number of programs (clients + server)
    int nPrograms;

    /// Id of the program run with this rank
    int programId;

    /// This rank is an MCL server
    bool rankIsServer;

    /// This rank is an MCL client
    bool rankIsClient;

public:

    Transport() = default;

    virtual ~Transport() = default;

    /// Initialize clients and server for receiving/sending data 
    /**
     * \param ...
     */
    virtual int initialize(void *args) = 0;

    /// Establish connection to a specified address
    /**
     * \param id id of the client to be connected
     */
    virtual int connect(int id) = 0;

    /// Accept a connection from a specified address
    /**
     * \param id id of the client to accept connection from
     */
    virtual int acceptConnection(int id) = 0;

    /// Disconnect client
    /**
     * \param id id of the client to disconnect
     */
    virtual void closeConnection (int id) = 0;

    /// Analogue to MPI_Probe return the size of the message waiting in the queue
    /**
     * \param id id of the client
     * \param data_type type of data to be received
     */
    virtual int probe(int id, int data_type) = 0;

    /// Destroy the port associated with the specified path
    virtual void destroy(int id) = 0;

    /// Send the data to a specified client
    /**
     * \param data pointer to the array containing data to be sent
     * \param count number of elements in the data array
     * \param data_type type of the data to be sent
     * \param tag message tag that used for matching
     * \param dest id of the receiving client
     */
    virtual void send(void *data, int count, int data_type, int tag, int dest) = 0;

    /// Receive data from the endpoint
    /**
     * \param data pointer to the array containing data to be sent
     * \param count number of elements in the data array
     * \param data_type type of the data to be received
     * \param tag message tag that used for matching
     * \param source id of the sending endpoint
     */
    virtual void receive(void *data, int count, int data_type, int tag, int source) = 0;

    virtual bool isServer() const { return rankIsServer; };
    virtual bool isClient() const { return rankIsClient; };
    
    virtual int getNPrograms() const { return nPrograms; };
    virtual int getProgramId() const { return programId; };

};

}

#endif // MCL_TRANSPORT_TRANSPORT_H_
