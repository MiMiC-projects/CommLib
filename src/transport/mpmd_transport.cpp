//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mpmd_transport.h"

#include "data_types.h"
#include "error_handler.h"

namespace mcl {

int MPMDTransport::initialize(void *args) {
    // communication tags
    static constexpr int CLIENT_ID_TAG = 12345;
    static constexpr int CLIENT_LIST_TAG = 12346;

    // split the world communicator
    int result;

    int *color;
    int flag;
    MPI_Comm_get_attr(worldComm, MPI_APPNUM, &color, &flag);
    if (flag) {
        result = MPI_Comm_split(worldComm, *color, 0, &localComm);
        if (result != MPI_SUCCESS)
            handle_error(ERROR_FATAL, "MPMDTransport: World comminicator could not be split!", EARGS);

        result = MPI_Comm_dup(localComm, static_cast<MPI_Comm *>(args));
        if (result != MPI_SUCCESS)
            handle_error(ERROR_FATAL, "MPMDTransport: Split communicator could not be set!", EARGS);

        programId = *color;
    }

    int worldRank, localRank;
    MPI_Comm_rank(worldComm, &worldRank);
    MPI_Comm_rank(localComm, &localRank);
  
    MPI_Allreduce(&programId, &nPrograms, 1, MPI_INT, MPI_MAX, worldComm);
    nPrograms++;
    
    std::vector<int> intercommRanks(nPrograms);

    if (worldRank == 0) {
        // identify clients
        intercommRanks[0] = worldRank;
        for (size_t i = 1; i < (size_t) nPrograms; i++) {
            MPI_Status status;
            int clientId, remoteClient;
            
            MPI_Probe(MPI_ANY_SOURCE, CLIENT_ID_TAG, worldComm, &status);
            remoteClient = status.MPI_SOURCE;
            MPI_Recv(&clientId, 1, MPI_INT, remoteClient, CLIENT_ID_TAG, worldComm, &status);
            intercommRanks[clientId] = remoteClient;
        }

        // send client list
        for (size_t i = 1;  i < (size_t) nPrograms; i++) {
            MPI_Send(&(*intercommRanks.begin()), nPrograms, MPI_INT, intercommRanks[i], CLIENT_LIST_TAG, worldComm);
        }
    } else if (localRank == 0) {
        MPI_Status status;
        MPI_Send(&programId, 1, MPI_INT, 0, CLIENT_ID_TAG, worldComm);
        MPI_Recv(&(*intercommRanks.begin()), nPrograms, MPI_INT, 0, CLIENT_LIST_TAG, worldComm, &status);
    }
    
    if (localRank == 0) {
        MPI_Group world;
        MPI_Comm_group(worldComm, &world);
        MPI_Group intercommGroup;
        MPI_Group_incl(world, nPrograms, &(*intercommRanks.begin()), &intercommGroup);
        MPI_Comm interComm;
        MPI_Comm_create_group(worldComm, intercommGroup, 1, &interComm);
        interComms.push_back(interComm);
    }

    rankIsServer = worldRank == 0;
    rankIsClient = localRank == 0 && worldRank != 0;
    
    return 0;

}

void MPMDTransport::send(void *data, int count, int data_type, int tag, int dest) {
    MPI_Send(data, count, pick_mpi_type(data_type), dest, tag, interComms[0]);
}

void MPMDTransport::receive(void *data, int count, int data_type, int tag, int source) {
    MPI_Status status;
    MPI_Recv(data, count, pick_mpi_type(data_type), source, tag, interComms[0], &status);
}

int MPMDTransport::probe(int id, int data_type) {
    int size;
    MPI_Status status;
    MPI_Probe(id, MPI_ANY_TAG, interComms[0], &status);
    MPI_Get_count(&status, pick_mpi_type(data_type), &size);
    return size;
}

MPI_Datatype MPMDTransport::pick_mpi_type(int data_type) {
    MPI_Datatype mpi_type;
    if (data_type == TYPE_CHAR) {
        mpi_type = MPI_CHAR;
    } else if (data_type == TYPE_INT) {
        mpi_type = MPI_INT;
    } else if (data_type == TYPE_LONG_INT) {
        mpi_type = MPI_LONG;
    } else if (data_type == TYPE_FLOAT) {
        mpi_type = MPI_FLOAT;
    } else if (data_type == TYPE_DOUBLE) {
        mpi_type = MPI_DOUBLE;
    } else {
        mpi_type = MPI_CHAR;
        handle_error(ERROR_WARNING, "MPMDTransport: Unknown type 'TYPE_FIX' chosen! "
                                    "Transferring the message as 'TYPE_CHAR'!", EARGS);
    }
    return mpi_type;
}

} // namespace mcl
