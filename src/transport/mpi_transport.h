//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_TRANSPORT_MPITRANSPORT_H_
#define MCL_TRANSPORT_MPITRANSPORT_H_

#include <future>
#include <iostream>

#include "mpi.h"

#include "error_handler.h"
#include "transport.h"

namespace mcl {

/**
 * Transport implementation using MPI intercommunicators
 */
class MPITransport : public Transport {

private:

    class Port {
     public: 
        typedef char TMPIPortName[MPI_MAX_PORT_NAME];
        TMPIPortName name;
        bool isOpen;
    };

    /// Path to the directory where portfiles will be written
    std::string portDir;

    /// Array of ports used to connect clients
    std::vector<Port> ports;

    /// Communicator to which a remote group will be attached
    MPI_Comm selfComm = MPI_COMM_SELF;
    
    /// World communicator of a given program
    MPI_Comm worldComm;

    /// Array of intercommunicators that will handle connections
    std::vector<MPI_Comm> interComms;
    
    /// Name of the file used to share port address
    constexpr static auto PORT_FILENAME = ".MCL_portname";

    /// Name of the file used to share number of programs
    constexpr static auto NPROG_FILENAME = ".MCL_numprogs";

    /// 
    static constexpr auto TIMEOUT_KEY = "MCL_TIMEOUT";
    
    /// 
    static constexpr unsigned int TIMEOUT_DEFAULT = 5000u;
    
    /// 
    int timeout;

    /// 
    void setTimeout();

    /// 
    template <typename _future>
    void checkTimeout(_future &request, Port &port) {
        if (request.wait_for(std::chrono::milliseconds(timeout)) == std::future_status::timeout) {
            handle_error(ERROR_FATAL, "Connection timeout on port: " + std::string(port.name), EARGS);
        }
    }
    
    /// 
    static MPI_Datatype pick_mpi_type(int data_type);

public:

    explicit MPITransport(MPI_Comm comm) : Transport(), worldComm(comm) { }

    ~MPITransport() override = default;

    int initialize(void *args) override;

    void send(void *data, int count, int data_type, int tag, int dest) override;

    void receive(void *data, int count, int data_type, int tag, int source) override;

    int probe(int id, int data_type) override;

    int connect(int id) override;

    int acceptConnection(int id) override;

    void closeConnection(int id) override;

    void destroy(int id) override;

};

}

#endif // MCL_TRANSPORT_MPITRANSPORT_H_
