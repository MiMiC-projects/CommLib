//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_MCLC_H_
#define MCL_MCLC_H_

#include "mcl_requests.h"

/// External C and C++ API of the library
/**
 * \file mcl_c.h
 * NOTE: Currently all api calls are blocking!!!
 */


/// Initialize communicator. SHOULD BE CALLED AFTER MPI_INIT!!!
/**
 * \param param parameter or null
 * \return
 */
#ifdef __cplusplus
extern "C"
#endif
int MCL_init(void* param);

/// Destroy the endpoint
#ifdef __cplusplus
extern "C"
#endif
void MCL_destroy();

/// Get the number of running programs
#ifdef __cplusplus
extern "C"
#endif
int MCL_Get_program_num();

/// Get the program's id from MCL
#ifdef __cplusplus
extern "C"
#endif
int MCL_Get_program_id();

/// Send data to specified client
/**
 * \param buffer pointer to the data buffer
 * \param length number of entities to send
 * \param data_type type of data to send
 * \param tag message tag that used for matching
 * \param destination id of the client to receive data
 */
#ifdef __cplusplus
extern "C"
#endif
void MCL_send(void* buffer, int length, int data_type, int tag, int destination);

#ifdef __cplusplus
void MCL_send(char* buffer, int length, int tag, int destination);
void MCL_send(int* buffer, int length, int tag, int destination);
void MCL_send(long int* buffer, int length, int tag, int destination);
void MCL_send(float* buffer, int length, int tag, int destination);
void MCL_send(double* buffer, int length, int tag, int destination);
#endif

 /// Receive data from a specified client
 /**
 * \param buffer buffer to store data
 * \param length number of entities to receive
 * \param data_type type of data to send
 * \param tag message tag that used for matching
 * \param source id of the client which is sending data
 */
#ifdef __cplusplus
extern "C"
#endif
void MCL_receive(void* buffer, int length, int data_type, int tag, int source);
#ifdef __cplusplus
void MCL_receive(char* buffer, int length, int tag, int source);
void MCL_receive(int* buffer, int length, int tag, int source);
void MCL_receive(long int* buffer, int length, int tag, int source);
void MCL_receive(float* buffer, int length, int tag, int source);
void MCL_receive(double* buffer, int length, int tag, int source);
#endif

/// Tags for data type implemented in the framework
#ifdef __cplusplus
extern "C" const int MCL_CHAR;
extern "C" const int MCL_INT;
extern "C" const int MCL_LONG_INT;
extern "C" const int MCL_FLOAT;
extern "C" const int MCL_DOUBLE;
#else
extern const int MCL_CHAR;
extern const int MCL_INT;
extern const int MCL_LONG_INT;
extern const int MCL_FLOAT;
extern const int MCL_DOUBLE;
#endif

#endif // MCL_MCLC_H_
