//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_APIFORTRAN_INITWRAPPER_H_
#define MCL_APIFORTRAN_INITWRAPPER_H_

/**
 * \fn int MCL_init_wrap(int *fortran_comm)
 * Wrapper function for Fortran code to call
 * to convert Fortran MPI types to C
 * @param fcomm Fortran MPI communicator or null
 */
extern "C"
int MCL_init_wrap(int *fortran_comm);

#endif // MCL_APIFORTRAN_INITWRAPPER_H_
