//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_UTILS_H_
#define MCL_UTILS_H_

#include <string>

namespace mcl {

/// Returns an integer value assigned to an environment variable
int get_env_var_int (const std::string &var_name, int default_val = -100);

/// Returns a string assigned to an environment variable
std::string get_env_var_str (const std::string &var_name, bool required = false);

/// Returns true if string represents an integer
bool represents_integer(const std::string &input);

} // namespace mcl


#endif // MCL_UTILS_H_
