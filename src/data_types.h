//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_DATATYPES_H_
#define MCL_DATATYPES_H_

#define PP_TYPE_CHAR      PP_TYPE_CHAR_VALUE
#define PP_TYPE_INT       PP_TYPE_INT_VALUE
#define PP_TYPE_LONG_INT  PP_TYPE_LONG_INT_VALUE
#define PP_TYPE_FLOAT     PP_TYPE_FLOAT_VALUE
#define PP_TYPE_DOUBLE    PP_TYPE_DOUBLE_VALUE

// contains data types supported by the library

const int TYPE_CHAR     = PP_TYPE_CHAR;

const int TYPE_INT      = PP_TYPE_INT;

const int TYPE_LONG_INT = PP_TYPE_LONG_INT;

const int TYPE_FLOAT    = PP_TYPE_FLOAT;

const int TYPE_DOUBLE   = PP_TYPE_DOUBLE;

#endif // MCL_DATATYPES_H_
