//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_MAINCLASS_H_
#define MCL_MAINCLASS_H_

#include <cassert>
#include <iterator>
#include <memory>
#include <sstream>

#include "endpoint/client.h"
#include "endpoint/endpoint.h"
#include "endpoint/server.h"

namespace mcl {

/// Singleton main class of the MCL - mostly needed in order to simplify testing
class MCLMain {

private:
    std::shared_ptr<Endpoint> endpoint;
    std::shared_ptr<Transport> protocol;
    bool initialized = false;

    MCLMain() = default;

public:

    void operator=(MCLMain const&) = delete;

    /// Retrieve or create the singleton instance
    static MCLMain& getInstance() {
        static MCLMain instance;
        return instance;
    }

    void setProtocol(std::shared_ptr<Transport> protocol) {
        this->protocol = protocol;
    }

    int initialize(void *arg) {
        int result;

        result = protocol->initialize(arg);
        if (result != 0)
            handle_error(ERROR_FATAL, "The communication protocol failed to initialize!", EARGS);

        int nProg = protocol->getNPrograms();
        if (nProg < 2)
            handle_error(ERROR_FATAL, "A simulation is running with less than two programs. "
                                      "Please run with at least two!", EARGS);

        initialized = true;
        
        endpoint = nullptr;
        if (protocol->isServer()) {
            endpoint = std::make_shared<Server>(protocol);
        } else if (protocol->isClient()) {
            endpoint = std::make_shared<Client>(protocol);
        }

        return 0;
    }

    /// Send data to specified client
    /**
     * \param data pointer to the buffer with data
     * \param count number of entities to send
     * \param data_type type of data to send
     * \param tag message tag that used for matching
     * \param destination id of the client to receive data
     */
    void send(void *data, int count, int data_type, int tag, int destination) {
        if (!initialized) {
            handle_error(ERROR_FATAL, "It seems that MCL was not initialized properly. "
                                      "Please call MCL_Init first!", EARGS);
        }
        if (endpoint != nullptr)
            endpoint->send(data, count, destination, data_type, tag);
    }

    /// Receive data from a specified client
    /**
     * \param buffer buffer to store data
     * \param count number of entities to receive
     * \param data_type type of data to send
     * \param tag message tag that used for matching
     * \param source id of the client which is sending data
     */
    void receive(void *buffer, int count, int data_type, int tag, int source) {
        if (!initialized) {
            handle_error(ERROR_FATAL, "It seems that MCL was not initialized properly. "
                                      "Please call MCL_Init first!", EARGS);
        }
        if (endpoint != nullptr)
            endpoint->receive(buffer, count, source, data_type, tag);
    }

    int getProgramNum() {
        if (!initialized) {
            handle_error(ERROR_FATAL, "It seems that MCL was not initialized properly. "
                                      "Please call MCL_Init first!", EARGS);
        }
        return protocol->getNPrograms();
    }

    int getProgramId() {
        if (!initialized) {
            handle_error(ERROR_FATAL, "It seems that MCL was not initialized properly. "
                                      "Please call MCL_Init first!", EARGS);
        }
        return protocol->getProgramId();
    }

    /// Destroy the endpoint
    void destroy() {
        if (!initialized) {
            handle_error(ERROR_FATAL, "It seems that MCL was not initialized properly. "
                                      "Please call MCL_Init first!", EARGS);
        }
        if (endpoint != nullptr)
            endpoint->destroy();
    }

    const std::shared_ptr<Endpoint> &getEndpoint() const { return endpoint; }

};

}

#endif // MCL_MAINCLASS_H_
