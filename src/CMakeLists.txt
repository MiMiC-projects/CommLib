include(preproc_settings)
configure_file(
  mcl_config.h.in
  mcl_config.h
)
add_custom_command(
  OUTPUT mcl_requests.cpp mcl_requests.h mcl_requests.inc
  DEPENDS generate_requests.py REQUESTS
  COMMAND ${Python_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/generate_requests.py
                               ${CMAKE_CURRENT_SOURCE_DIR}/REQUESTS
                               "source"
  COMMENT "Generating request source codes"
)
set(SOURCE_FILES
  error_handler.cpp
  mcl_c.cpp
  mcl_requests.cpp
  utils.cpp
  api_fortran/init_wrapper.cpp
  endpoint/server.cpp
  endpoint/client.cpp
  transport/mpi_transport.cpp
  transport/mpmd_transport.cpp
)
set(HEADER_FILES
  mcl_c.h
  ${CMAKE_CURRENT_BINARY_DIR}/mcl_requests.h
  ${CMAKE_CURRENT_BINARY_DIR}/mcl_config.h
)
add_library(mimiccomm ${SOURCE_FILES})
set_target_properties(
  mimiccomm
  PROPERTIES
  POSITION_INDEPENDENT_CODE ${BUILD_SHARED_LIBS}
  VERSION ${PROJECT_VERSION}
  SOVERSION ${PROJECT_VERSION}
)
target_include_directories(
  mimiccomm
  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/transport>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/endpoint>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
  PUBLIC
    ${MPI_CXX_INCLUDE_DIRS}
)
target_link_libraries(
  mimiccomm
  INTERFACE
    MPI::MPI_CXX
    stdc++fs
)
install(
  TARGETS mimiccomm
  DESTINATION ${CMAKE_INSTALL_LIBDIR}
  EXPORT MiMiCCommLibTargets
  COMPONENT CXX
)
install(
  FILES ${HEADER_FILES}
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
  COMPONENT CXX
)
if(BUILD_FORTRAN_API)
  set(FORTRAN_SOURCE_FILES
    api_fortran/mcl_fortran.F90
    api_fortran/c_bindings.F90
  )
  set(FORTRAN_INCLUDE_FILES
    mcl_requests.inc
  )
  set(FORTRAN_MODULE_FILES
    ${CMAKE_Fortran_MODULE_DIRECTORY}/mcl_fortran.mod
  )
  add_library(mimiccommf
    ${FORTRAN_SOURCE_FILES}
    ${FORTRAN_INCLUDE_FILES}
  )
  set_target_properties(
    mimiccommf
    PROPERTIES
    POSITION_INDEPENDENT_CODE ${BUILD_SHARED_LIBS}
    VERSION ${PROJECT_VERSION}
    SOVERSION ${PROJECT_VERSION}
  )
  target_include_directories(
    mimiccommf
    PUBLIC
      $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
      $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
      $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
    PUBLIC
      ${MPI_Fortran_INCLUDE_DIRS}
  )
  install(
    TARGETS mimiccommf
    DESTINATION ${CMAKE_INSTALL_LIBDIR}
    EXPORT MiMiCCommLibTargets
    COMPONENT Fortran
  )
  install(
    FILES ${FORTRAN_MODULE_FILES}
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
    COMPONENT Fortran
  )
endif()
include(CMakePackageConfigHelpers)
set(INCLUDE_INSTALL_DIR ${CMAKE_INSTALL_INCLUDEDIR})
configure_package_config_file(
  ${CMAKE_SOURCE_DIR}/cmake/MiMiCCommLibConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/MiMiCCommLibConfig.cmake
  INSTALL_DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${CMAKE_PROJECT_NAME}
  PATH_VARS INCLUDE_INSTALL_DIR
)
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/MiMiCCommLibConfigVersion.cmake
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY SameMajorVersion
)
install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/MiMiCCommLibConfig.cmake
  ${CMAKE_CURRENT_BINARY_DIR}/MiMiCCommLibConfigVersion.cmake
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${CMAKE_PROJECT_NAME}
  COMPONENT CXX
)
install(
  EXPORT MiMiCCommLibTargets
  FILE MiMiCCommLibTargets.cmake
  NAMESPACE MiMiCCommLib::
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${CMAKE_PROJECT_NAME}
)
export(
  EXPORT MiMiCCommLibTargets
  FILE ${CMAKE_BINARY_DIR}/cmake/MiMiCCommLibTargets.cmake
  NAMESPACE MiMiCCommLib::
)

