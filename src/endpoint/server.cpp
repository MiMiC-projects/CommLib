//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "server.h"

#include <cassert>

#include "client.h"
#include "error_handler.h"
#include "data_types.h"

namespace mcl {

int Server::init() {
    setId(0);
    size_t nPrograms = static_cast<size_t>(protocol->getNPrograms());
    for (size_t i = 1; i < nPrograms; i++) {
        client_list.push_back(std::make_unique<Client>(i, protocol));

        int result = protocol->acceptConnection(i);
        if (result != 0)
            handle_error(ERROR_FATAL, "Server::Init : Connection was not successful", EARGS);

        
        // check that the ID is correctly assigned
        int clientId;
        protocol->receive(&clientId, 1, TYPE_INT, CLIENT_ID_TAG, i);
        if (clientId != static_cast<int>(i)) {
            handle_error(ERROR_FATAL, "Server::Init : Received client ID does not match its "
                                      "internal ID in the server's client list.", EARGS);
        }
    }

    return 0;
}

void Server::disconnect(int dest) {
    protocol->closeConnection(dest);
}

void Server::destroy() {
    for (size_t i = 0; i < client_list.size(); ++i) {
        disconnect(i+1);
        protocol->destroy(i+1);
    }
}

} // namespace mcl
