//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_ENDPOINT_SERVER_H_
#define MCL_ENDPOINT_SERVER_H_

#include "endpoint.h"

namespace mcl {

/**
 * Endpoint representing server
 */
class Server : public Endpoint {

private:

    int init();

public:

    Server(std::shared_ptr<Transport> protocol) : Endpoint(protocol) { init(); }

    Server(int id, std::shared_ptr<Transport> protocol) : Endpoint(protocol, id) { }

    void disconnect(int dest) override;

    void destroy() override;
};

}

#endif // MCL_ENDPOINT_SERVER_H_
