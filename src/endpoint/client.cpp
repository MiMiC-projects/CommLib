//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "client.h"

#include "data_types.h"
#include "server.h"

namespace mcl {

int Client::init() {
    int programId = protocol->getProgramId();
    setId(programId);
    client_list.push_back(std::make_unique<Server>(0, protocol));

    protocol->connect(0);

    // send the ID to server to check that it is correctly assigned
    int clientId = programId;
    protocol->send(&clientId, 1, TYPE_INT, CLIENT_ID_TAG, 0);
    
    return 0;
}

void Client::disconnect(int dest) {
    protocol->closeConnection(dest);
}

void Client::destroy() {
    disconnect(0);
}

} // namespace mcl
