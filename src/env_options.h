//    MCL: MiMiC Communication Library
//    Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).
//
//    This file is part of MCL.
//
//    MCL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    MCL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MCL_ENVOPTIONS_H_
#define MCL_ENVOPTIONS_H_

// contains options used to control MCL through environment variables 

/// string key used to retrieve communication mechanism environment variable.
static constexpr auto COMMUNICATION_KEY = "MCL_COMM";

/// switch to MPI client-server mechanism
static constexpr int MCL_TR_MPI = 1;

/// switch to MPI MPMD mechanism
static constexpr int MCL_TR_MPMD = 2;

#endif // MCL_ENVOPTIONS_H_
