MiMiC Communication Library    {#mainpage}
===========================

The MiMiC Communication Library (MCL) enables communication between external
programs coupled through the MiMiC framework. See https://mimic-project.org/
for further information.

Copyright and License
---------------------

Copyright (C) 2015-2025  The MiMiC Contributors (see CONTRIBUTORS file for details).

MCL is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

MCL is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Software Requirements
---------------------

- CMake 3.12+
- Python 3.6+
- C++ compiler (C++17 standard)
- Fortran compiler (Fortran 2008 standard) (see [Fortran API](README.md#fortran-api))
- MPI library (MPI 2.1 standard)

Compilation
-----------

An example of a compilation procedure is shown below:

```bash
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=<path-to-install> <path-to-mcl-source>
make
make install
```

The communication mode is governed by the `MCL_COMM` environment variable:

- `MCL_COMM=1` will choose the MPI-based client-server mechanism.
- `MCL_COMM=2` will choose the MPI multiple-program multiple-data (MPMD)
  communicator (this is the default setting).

Unit Tests
----------

Building and running unit tests requires [GoogleTest](https://github.com/google/googletest).
Note that you may need to provide the path to the CMake module directory of
GoogleTest CMake module using the `CMAKE_PREFIX_PATH` CMake option. Building
tests is an optional step enabled by the `-DBUILD_TESTS=ON` CMake option,
for example:

```bash
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug -DBUILD_TESTS=ON ..
make
ctest --output-on-failure
```

Usage
-----

The communication procedure with MCL is rather simple:

1. The communication layer has to be initialized by [MCL_init](@ref MCL_init).
   NOTE: this function must be called before any collective on
   `MPI_COMM_WORLD` is invoced as this will result in a deadlock! If the
   MPI MPMD communication mechanism is chosen then one needs to pass the
   `MPI_COMM_WORLD` copy into the function. Otherwise, `nullptr` will also
   work.
2. The handshake procedure is performed using [MCL_handshake](@ref MCL_handshake).
   Keep in mind that there can be only one server but multiple clients.

After these two steps the communicator is ready to be used by
[MCL_send](@ref MCL_send) and [MCL_receive](@ref MCL_send)
(see function documentation for details).

Fortran API
-----------

MCL also provides a Fortran 2008 compliant API that can be used in a
Fortran-based program. Be sure to include `mcl_fortran.mod` in the
compilation procedure and to link additionally against `libmclfortran.a`
(static) or `libmclfortran.so` (shared). The Fortran API can be disabled by
adding the `-DBUILD_FORTRAN_API=OFF` CMake option in the configuration step.

Running Server and Client(s)
----------------------------

Execution of the server and client programs linked against the MCL depends on
the communication mode chosen. If MPI MPMD is used, then the programs
can be run as follows:

```bash
mpirun -np X <program1> [<args1>] : -np Y <program2> [<args2>]
```

If the MPI-based client-server mechanism is used instead, then the command
will look like this:

```bash
mpirun -np X <program1> [<args1>] &
mpirun -np Y <program2> [<args2>] &
wait
```

Communication Protocol
----------------------

MiMiC implements a request-based client-server communication protocol.
MiMiC is the server that issues commands (encoded as integer constants,
tagged with [MCL_COMMAND](@ref MCL_COMMAND) tag) to the client. The client
waits until a command is received (via a [MCL_receive](@ref MCL_receive)
function call) after which it will perform the requested action (send
forces, compute forces, etc.). Sending/receiving data should be tagged with
an [MCL_DATA](@ref MCL_DATA) tag. The most important commands that every
client should be able to respond to are:

- [MCL_SEND_APIVER](@ref MCL_SEND_APIVER) -- send the version of the MCL API used by the client
- [MCL_SEND_NUM_COMMANDS](@ref MCL_SEND_NUM_COMMANDS) -- send the number of supported commands
- [MCL_SEND_COMMANDS](@ref MCL_SEND_COMMANDS) -- send the list of supported commands (as an integer array)
- [MCL_EXIT](@ref MCL_EXIT) -- exit the program

